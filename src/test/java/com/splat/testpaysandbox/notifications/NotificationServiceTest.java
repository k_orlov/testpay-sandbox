package com.splat.testpaysandbox.notifications;

import com.splat.testpaysandbox.notifications.model.Notification;
import com.splat.testpaysandbox.payments.model.Amount;
import com.splat.testpaysandbox.payments.model.Currencies;
import com.splat.testpaysandbox.payments.model.PaymentStates;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@RunWith(SpringRunner.class)
@ContextConfiguration(
        classes = {
                NotificationService.class,
                NotificationServiceTest.Config.class
        }
)
public class NotificationServiceTest {
    @Autowired
    private NotificationService srv;
    @Autowired
    private NotificationProps props;

    @MockBean
    private RestTemplate restTemplate;


    @Test
    public void notify_successCase() throws InterruptedException {
        Mockito.when(restTemplate.postForEntity(Mockito.anyString(), Mockito.isNull(), Mockito.eq(String.class), Mockito.<Map<String, ?>>any()))
                .thenReturn(new ResponseEntity<>("OK", HttpStatus.OK));

        srv.addNotification(new Notification(
                        "pId",
                        PaymentStates.CREATED,
                        "eTxId",
                        new Amount("42", Currencies.USD),
                        "http://test.com/callback"
                )
        );

        Thread.sleep(50);

        Mockito.verify(restTemplate).postForEntity(Mockito.anyString(), Mockito.isNull(), Mockito.eq(String.class), Mockito.<Map<String, ?>>any());
    }

    @Test
    public void notify_retryCase() throws InterruptedException {
        Mockito.when(restTemplate.postForEntity(Mockito.anyString(), Mockito.isNull(), Mockito.eq(String.class), Mockito.<Map<String, ?>>any()))
                .thenReturn(new ResponseEntity<>("Error", HttpStatus.INTERNAL_SERVER_ERROR));

        srv.addNotification(new Notification(
                        "pId",
                        PaymentStates.CREATED,
                        "eTxId",
                        new Amount("42", Currencies.USD),
                        "http://test.com/callback"
                )
        );

        Thread.sleep(100);

        Mockito.verify(restTemplate, Mockito.times(props.getMaxRetryCount()))
                .postForEntity(Mockito.anyString(), Mockito.isNull(), Mockito.eq(String.class), Mockito.<Map<String, ?>>any());
    }

    @Test
    public void notify_checkSign() throws InterruptedException {
        String expectedPaymentId = "26a583aa-03f2-4d3c-9815-6c88620e2675";
        String expectedExternalId = "123456789";
        Amount expectedAmount = new Amount("42", Currencies.USD);

        Mockito.when(restTemplate.postForEntity(Mockito.anyString(), Mockito.isNull(), Mockito.eq(String.class), Mockito.<Map<String, ?>>any()))
                .thenReturn(new ResponseEntity<>("OK", HttpStatus.OK));

        srv.addNotification(new Notification(
                        expectedPaymentId,
                        PaymentStates.CREATED,
                        expectedExternalId,
                        expectedAmount,
                        "http://test.com/callback"
                )
        );

        Thread.sleep(50);


        Mockito.verify(restTemplate).postForEntity(
                Mockito.anyString(),
                Mockito.isNull(),
                Mockito.eq(String.class),
                Mockito.<Map<String, String>>argThat(m -> "5B336E2B30286347CB95C7C95473DBEF058F0B34328996BC8ADF75828B0EF397".equals(m.get("sign")))
        );
    }

    @TestConfiguration
    public static class Config {
        @Bean
        public ScheduledExecutorService scheduledExecutorService() {
            return Executors.newScheduledThreadPool(1);
        }

        @Bean
        public NotificationProps props() {
            NotificationProps props = new NotificationProps();
            props.setMaxRetryCount(3);
            props.setRetryDelay(Duration.ofMillis(30));
            props.setSecret("secret");
            return props;
        }
    }
}