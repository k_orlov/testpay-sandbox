package com.splat.testpaysandbox.payments;

import com.splat.testpaysandbox.notifications.NotificationService;
import com.splat.testpaysandbox.notifications.model.Notification;
import com.splat.testpaysandbox.payments.model.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Duration;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@RunWith(SpringRunner.class)
@ContextConfiguration(
        classes = {
                PaymentService.class,
                PaymentServiceTest.Config.class
        }
)
public class PaymentServiceTest {

    @MockBean
    private ProbabilityService probabilityService;
    @MockBean
    private NotificationService notificationService;

    @Autowired
    private PaymentService srv;
    @Autowired
    private PaymentProps props;

    private final Payer testPayer = new Payer("test@mail.com");
    private final Transaction testTx = new Transaction(
            new Amount("42", Currencies.USD),
            UUID.randomUUID().toString(),
            null
    );

    @Test
    public void createPayment_successCase() throws InterruptedException {
        Mockito.when(probabilityService.isHappen(props.getErrorProbability())).thenReturn(false);
        Mockito.when(probabilityService.isHappen(props.getAcceptanceProbability())).thenReturn(true);
        Payment payment = srv.createPayment(Intents.ORDER, testPayer, testTx, "http://test.com/callback");

        Thread.sleep(props.getDecisionTimeMax().toMillis() + 10);

        Mockito.verify(notificationService)
                .addNotification(
                        new Notification(
                                payment.getId(),
                                PaymentStates.CREATED,
                                testTx.getExternalId(),
                                testTx.getAmount(),
                                payment.getNotificationUrl()
                        )
                );

        Mockito.verify(notificationService)
                .addNotification(
                        new Notification(
                                payment.getId(),
                                PaymentStates.APPROVED,
                                testTx.getExternalId(),
                                testTx.getAmount(),
                                payment.getNotificationUrl()
                        )
                );
    }

    @Test(expected = RuntimeException.class)
    public void createPayment_serviceErrorCase() {
        Mockito.when(probabilityService.isHappen(props.getErrorProbability())).thenReturn(true);
        srv.createPayment(Intents.ORDER, testPayer, testTx, "http://test.com/callback");
    }

    @TestConfiguration
    public static class Config {
        @Bean
        public ScheduledExecutorService scheduledExecutorService() {
            return Executors.newScheduledThreadPool(1);
        }

        @Bean
        public PaymentProps props() {
            PaymentProps props = new PaymentProps();
            props.setAcceptanceProbability(0.9);
            props.setErrorProbability(0.01);
            props.setDecisionTimeMin(Duration.ofMillis(10));
            props.setDecisionTimeMax(Duration.ofMillis(50));
            return props;
        }
    }
}