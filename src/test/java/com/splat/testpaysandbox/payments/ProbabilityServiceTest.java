package com.splat.testpaysandbox.payments;

import org.junit.Assert;
import org.junit.Test;

public class ProbabilityServiceTest {

    private final ProbabilityService srv = new ProbabilityService();

    @Test
    public void isHappen() {
        int probesCount = 1_000_000;
        double expected = 0.891;

        int happenTimes = 0;
        for (int i = 0; i < probesCount; i++) {
            if (srv.isHappen(expected)) {
                happenTimes++;
            }
        }

        double actual = 1.0 * happenTimes / probesCount;
        Assert.assertTrue(
                String.format("Delta should be less or equals %f. Actual is %f, expected is %f", ProbabilityService.ACCURACY, actual, expected),
                Math.abs(actual - expected) <= ProbabilityService.ACCURACY
                );
    }
}