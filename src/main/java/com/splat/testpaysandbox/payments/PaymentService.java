package com.splat.testpaysandbox.payments;

import com.splat.testpaysandbox.notifications.NotificationService;
import com.splat.testpaysandbox.notifications.model.Notification;
import com.splat.testpaysandbox.payments.model.Intents;
import com.splat.testpaysandbox.payments.model.Payer;
import com.splat.testpaysandbox.payments.model.Payment;
import com.splat.testpaysandbox.payments.model.PaymentStates;
import com.splat.testpaysandbox.payments.model.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class PaymentService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentService.class);

    private final ScheduledExecutorService scheduler;
    private final ProbabilityService probabilityService;
    private final NotificationService notificationService;
    private final PaymentProps props;

    public PaymentService(ScheduledExecutorService scheduler, ProbabilityService probabilityService, NotificationService notificationService, PaymentProps props) {
        this.scheduler = scheduler;
        this.probabilityService = probabilityService;
        this.notificationService = notificationService;
        this.props = props;
    }

    private final Random random = new Random();

    public Payment createPayment(Intents intent, Payer payer, Transaction tx, String notificationUrl) {
        LOGGER.info("New payment: tx={}, notificationUrl={}", tx, notificationUrl);
        if (probabilityService.isHappen(props.getErrorProbability())) {
            LOGGER.info("Probability of errors is {}... and it happens", props.getErrorProbability());
            throw new RuntimeException("Time to crash");
        }

        Payment payment = new Payment(
                UUID.randomUUID().toString(),
                intent,
                notificationUrl,
                payer,
                tx,
                PaymentStates.CREATED,
                ZonedDateTime.now()
        );

        notificationService.addNotification(paymentToNotification(payment, null));

        long millisToDecision = props.getDecisionTimeMin().toMillis() + random.nextInt((int) props.getDecisionTimeMax().minus(props.getDecisionTimeMin()).toMillis());
        LOGGER.info("Going to make a decision for payment id={} in {} second(s)", payment.getId(), millisToDecision);
        scheduler.schedule(() -> takeDecision(payment), millisToDecision, TimeUnit.MILLISECONDS);

        return payment;
    }

    private void takeDecision(Payment payment) {
        PaymentStates state;
        if (probabilityService.isHappen(props.getAcceptanceProbability())) {
            state = PaymentStates.APPROVED;
        } else {
            state = PaymentStates.FAILED;
        }

        LOGGER.info("Payment has been updated: id={}, newState={}", payment.getId(), state);
        notificationService.addNotification(paymentToNotification(payment, state));
    }

    private Notification paymentToNotification(Payment payment, PaymentStates newState) {
        return new Notification(
                payment.getId(),
                newState != null ? newState : payment.getState(),
                payment.getTransaction().getExternalId(),
                payment.getTransaction().getAmount(),
                payment.getNotificationUrl()
        );
    }
}
