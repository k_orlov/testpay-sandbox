package com.splat.testpaysandbox.payments;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.time.Duration;

@ConfigurationProperties(prefix = "service.payment")
public class PaymentProps {
    private double errorProbability = 0.05;
    private double acceptanceProbability = 0.5;
    private Duration decisionTimeMin = Duration.ofSeconds(5);
    private Duration decisionTimeMax = Duration.ofSeconds(30);

    public double getErrorProbability() {
        return errorProbability;
    }

    public void setErrorProbability(double errorProbability) {
        this.errorProbability = errorProbability;
    }

    public double getAcceptanceProbability() {
        return acceptanceProbability;
    }

    public void setAcceptanceProbability(double acceptanceProbability) {
        this.acceptanceProbability = acceptanceProbability;
    }

    public Duration getDecisionTimeMin() {
        return decisionTimeMin;
    }

    public void setDecisionTimeMin(Duration decisionTimeMin) {
        this.decisionTimeMin = decisionTimeMin;
    }

    public Duration getDecisionTimeMax() {
        return decisionTimeMax;
    }

    public void setDecisionTimeMax(Duration decisionTimeMax) {
        this.decisionTimeMax = decisionTimeMax;
    }
}
