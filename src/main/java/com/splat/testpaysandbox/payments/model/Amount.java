package com.splat.testpaysandbox.payments.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class Amount {
    @NotNull
    @Size(min = 1, max = 10)
    @Pattern(regexp = "^\\d+(\\.\\d{1,2})?$")
    private final String value;
    private final Currencies currency;

    public Amount(
            @JsonProperty("value") String value,
            @JsonProperty("currency") Currencies currency
    ) {
        this.value = value;
        this.currency = currency;
    }

    public String getValue() {
        return value;
    }

    public Currencies getCurrency() {
        return currency;
    }
}
