package com.splat.testpaysandbox.payments.model;

import java.time.ZonedDateTime;

public class Payment {
    private final String id;
    private final Intents intent;
    private final String notificationUrl;
    private final Payer payer;
    private final Transaction transaction;
    private final PaymentStates state;
    private final ZonedDateTime created;

    public Payment(String id, Intents intent, String notificationUrl, Payer payer, Transaction transaction, PaymentStates state, ZonedDateTime created) {
        this.id = id;
        this.intent = intent;
        this.notificationUrl = notificationUrl;
        this.payer = payer;
        this.transaction = transaction;
        this.state = state;
        this.created = created;
    }

    public String getId() {
        return id;
    }

    public Intents getIntent() {
        return intent;
    }

    public String getNotificationUrl() {
        return notificationUrl;
    }

    public Payer getPayer() {
        return payer;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public PaymentStates getState() {
        return state;
    }

    public ZonedDateTime getCreated() {
        return created;
    }
}
