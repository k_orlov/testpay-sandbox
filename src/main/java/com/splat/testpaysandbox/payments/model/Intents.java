package com.splat.testpaysandbox.payments.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Intents {
    @JsonProperty("order")
    ORDER
}
