package com.splat.testpaysandbox.payments.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class Transaction {
    @Valid
    @NotNull
    private final Amount amount;
    private final String externalId;
    private final String description;

    public Transaction(
            @JsonProperty("amount") Amount amount,
            @JsonProperty("external_id") String externalId,
            @JsonProperty("description") String description
    ) {
        this.amount = amount;
        this.externalId = externalId;
        this.description = description;
    }

    public Amount getAmount() {
        return amount;
    }

    public String getExternalId() {
        return externalId;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "amount=" + amount +
                ", externalId='" + externalId + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
