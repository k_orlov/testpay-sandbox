package com.splat.testpaysandbox.payments.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum PaymentStates {
    @JsonProperty("created")
    CREATED,
    @JsonProperty("approved")
    APPROVED,
    @JsonProperty("failed")
    FAILED
}
