package com.splat.testpaysandbox.payments.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.splat.testpaysandbox.payments.model.Intents;
import com.splat.testpaysandbox.payments.model.Payer;
import com.splat.testpaysandbox.payments.model.Transaction;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Request for creating new payment
 */
public class CreatePaymentRequest {
    @NotNull
    private final Intents intent;
    @NotNull
    private final String notificationUrl;
    @Valid
    @NotNull
    private final Payer payer;
    @Valid
    @NotNull
    private final Transaction transaction;

    public CreatePaymentRequest(
            @JsonProperty("intent") Intents intent,
            @JsonProperty("notification_url") String notificationUrl,
            @JsonProperty("payer") Payer payer,
            @JsonProperty("transaction") Transaction transaction
    ) {
        this.intent = intent;
        this.notificationUrl = notificationUrl;
        this.payer = payer;
        this.transaction = transaction;
    }

    public Intents getIntent() {
        return intent;
    }

    public String getNotificationUrl() {
        return notificationUrl;
    }

    public Payer getPayer() {
        return payer;
    }

    public Transaction getTransaction() {
        return transaction;
    }
}
