package com.splat.testpaysandbox.payments.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.splat.testpaysandbox.payments.model.PaymentStates;

import java.time.ZonedDateTime;

/**
 * Response on create payment operation
 */
public class CreatePaymentResponse {
    private final String id;
    private final PaymentStates state;
    @JsonProperty("create_time")
    private final ZonedDateTime created;

    public CreatePaymentResponse(String id, PaymentStates state, ZonedDateTime created) {
        this.id = id;
        this.state = state;
        this.created = created;
    }

    public String getId() {
        return id;
    }

    public PaymentStates getState() {
        return state;
    }

    public ZonedDateTime getCreated() {
        return created;
    }
}
