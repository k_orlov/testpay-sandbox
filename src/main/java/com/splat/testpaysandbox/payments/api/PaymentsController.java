package com.splat.testpaysandbox.payments.api;

import com.splat.testpaysandbox.payments.PaymentService;
import com.splat.testpaysandbox.payments.model.Payment;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.ZonedDateTime;

@RestController
@RequestMapping(
        value = "/payments",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE
)
@Secured("ROLE_CUSTOMER")
public class PaymentsController {

    private final PaymentService paymentService;

    public PaymentsController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @PostMapping("/payment")
    public CreatePaymentResponse createPayment(@Valid @RequestBody CreatePaymentRequest req) {
        Payment payment = paymentService.createPayment(req.getIntent(), req.getPayer(), req.getTransaction(), req.getNotificationUrl());
        return new CreatePaymentResponse(payment.getId(), payment.getState(), ZonedDateTime.now());
    }
}
