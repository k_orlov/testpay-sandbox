package com.splat.testpaysandbox.payments;

import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class ProbabilityService {
    private static final int RANGE = 1_000_000;
    static final double ACCURACY = 1000.0 / RANGE; // good enough for sandbox

    private final Random rnd = new Random();

    public boolean isHappen(double probability) {
        if (probability < 0.0 || probability > 1.0) {
            throw new IllegalArgumentException("Probability should be in range 0..1");
        }
        return RANGE * probability >= rnd.nextInt(RANGE);
    }

}
