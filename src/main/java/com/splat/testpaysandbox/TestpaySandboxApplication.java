package com.splat.testpaysandbox;

import com.splat.testpaysandbox.auth.AuthProps;
import com.splat.testpaysandbox.notifications.NotificationProps;
import com.splat.testpaysandbox.payments.PaymentProps;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@SpringBootApplication
@EnableConfigurationProperties(
        value = {
                PaymentProps.class,
                NotificationProps.class,
                AuthProps.class
        }
)
public class TestpaySandboxApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestpaySandboxApplication.class, args);
    }

    private static final Duration SOCKET_TIMEOUT = Duration.ofSeconds(10);

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplateBuilder()
                .setConnectTimeout(SOCKET_TIMEOUT)
                .setReadTimeout(SOCKET_TIMEOUT)
                .build();
    }

    @Bean
    public ScheduledExecutorService scheduledExecutorService() {
        return Executors.newScheduledThreadPool(5);
    }
}
