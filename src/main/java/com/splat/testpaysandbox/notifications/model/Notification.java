package com.splat.testpaysandbox.notifications.model;

import com.splat.testpaysandbox.payments.model.Amount;
import com.splat.testpaysandbox.payments.model.PaymentStates;

import java.util.Objects;

/**
 * Notification about payments update
 */
public class Notification {
    private final String paymentId;
    private final String externalTxId;
    private final String notificationUrl;
    private final Amount amount;
    private final PaymentStates state;

    public Notification(String paymentId, PaymentStates state, String externalTxId, Amount amount, String notificationUrl) {
        this.paymentId = paymentId;
        this.externalTxId = externalTxId;
        this.notificationUrl = notificationUrl;
        this.amount = amount;
        this.state = state;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public String getExternalTxId() {
        return externalTxId;
    }

    public String getNotificationUrl() {
        return notificationUrl;
    }

    public Amount getAmount() {
        return amount;
    }

    public PaymentStates getState() {
        return state;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "paymentId='" + paymentId + '\'' +
                ", externalTxId='" + externalTxId + '\'' +
                ", notificationUrl='" + notificationUrl + '\'' +
                ", amount=" + amount +
                ", state=" + state +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Notification)) return false;
        Notification that = (Notification) o;
        return Objects.equals(paymentId, that.paymentId) &&
                Objects.equals(externalTxId, that.externalTxId) &&
                Objects.equals(notificationUrl, that.notificationUrl) &&
                Objects.equals(amount, that.amount) &&
                state == that.state;
    }

    @Override
    public int hashCode() {
        return Objects.hash(paymentId, externalTxId, notificationUrl, amount, state);
    }
}
