package com.splat.testpaysandbox.notifications;

import com.splat.testpaysandbox.notifications.model.Notification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class NotificationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationService.class);

    private final ScheduledExecutorService scheduler;
    private final RestTemplate restTemplate;
    private final NotificationProps props;
    private final String hashedSecret;

    public NotificationService(
            ScheduledExecutorService scheduler,
            RestTemplate restTemplate,
            NotificationProps props
    ) throws NoSuchAlgorithmException {
        this.scheduler = scheduler;
        this.restTemplate = restTemplate;
        this.props = props;

        MessageDigest md = MessageDigest.getInstance("SHA-256");
        this.hashedSecret = HexUtils.bytesToHex(md.digest(props.getSecret().getBytes()));
    }

    public void addNotification(Notification notification) {
        scheduler.schedule(() -> doNotify(new NotificationTask(notification)), 0, TimeUnit.MILLISECONDS);
    }

    private void doNotify(NotificationTask task) {
        Notification notification = task.notification;
        if (task.retries >= props.getMaxRetryCount()) {
            LOGGER.info("Task is overdue: task={}", task);
            return;
        }

        try {
            LOGGER.info("Going to notify with {}", task);
            String url = notification.getNotificationUrl().endsWith("/")
                    ? notification.getNotificationUrl().substring(0, notification.getNotificationUrl().length() - 1)
                    : notification.getNotificationUrl();
            Map<String, String> params = new HashMap<>();
            params.put("currency", notification.getAmount().getCurrency().toString());
            params.put("amount", notification.getAmount().getValue());
            params.put("externalId", notification.getExternalTxId());
            params.put("state", notification.getState().toString());
            params.put("sign", makeSign(notification));
            params.put("id", notification.getPaymentId());
            ResponseEntity<String> response = restTemplate.postForEntity(
                    url + "?currency={currency}&amount={amount}&id={id}&external_id={externalId}&status={state}&sha2sig={sign}",
                    null, String.class, params);

            if (response.getStatusCode() == HttpStatus.OK) {
                return;
            }
        } catch (Exception e) {
            LOGGER.error("Error during notification: {}", e.getMessage(), e);
        }

        // If we come here, then the task must be rescheduled
        LOGGER.info("Next attempt at {}, paymentId={}", ZonedDateTime.now().plusSeconds(props.getRetryDelay().getSeconds()), notification.getPaymentId());
        scheduler.schedule(() -> doNotify(task.next()), props.getRetryDelay().toMillis(), TimeUnit.MILLISECONDS);
    }

    private String makeSign(Notification notification) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        return HexUtils.bytesToHex(
                md.digest(
                        String.join(
                                "",
                                notification.getAmount().getCurrency().toString(),
                                notification.getAmount().getValue(),
                                hashedSecret,
                                notification.getPaymentId(),
                                notification.getExternalTxId(),
                                notification.getState().toString().toLowerCase()
                        ).getBytes()
                )
        );
    }

    private static class NotificationTask {
        private final Notification notification;
        private final ZonedDateTime created;
        private final int retries;

        public NotificationTask(Notification notification) {
            this.notification = notification;
            this.created = ZonedDateTime.now();
            this.retries = 0;
        }

        private NotificationTask(Notification notification, ZonedDateTime created, int retries) {
            this.notification = notification;
            this.created = created;
            this.retries = retries;
        }

        public NotificationTask next() {
            return new NotificationTask(notification, created, retries + 1);
        }

        @Override
        public String toString() {
            return "NotificationTask{" +
                    "notification=" + notification +
                    ", created=" + created +
                    ", retries=" + retries +
                    '}';
        }
    }
}
