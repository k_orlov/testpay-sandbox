package com.splat.testpaysandbox.auth;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.time.Duration;
import java.util.Collections;
import java.util.List;

@ConfigurationProperties(prefix = "service.auth")
public class AuthProps {
    private Duration defaultTokenTtl = Duration.ofMinutes(15);
    private List<UserConfiguration> users = Collections.emptyList();

    public Duration getDefaultTokenTtl() {
        return defaultTokenTtl;
    }

    public void setDefaultTokenTtl(Duration defaultTokenTtl) {
        this.defaultTokenTtl = defaultTokenTtl;
    }

    public List<UserConfiguration> getUsers() {
        return users;
    }

    public void setUsers(List<UserConfiguration> users) {
        this.users = users;
    }

    public static class UserConfiguration {
        private String clientId;
        private String secret;
        private Duration tokenTtl;

        public String getClientId() {
            return clientId;
        }

        public void setClientId(String clientId) {
            this.clientId = clientId;
        }

        public String getSecret() {
            return secret;
        }

        public void setSecret(String secret) {
            this.secret = secret;
        }

        public Duration getTokenTtl() {
            return tokenTtl;
        }

        public void setTokenTtl(Duration tokenTtl) {
            this.tokenTtl = tokenTtl;
        }
    }
}
