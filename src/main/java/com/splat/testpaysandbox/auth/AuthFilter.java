package com.splat.testpaysandbox.auth;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class AuthFilter extends GenericFilterBean {
    private static final String AUTHORIZATION_HEADER_NAME = "Authorization";

    private final AuthService authService;

    public AuthFilter(AuthService authService) {
        this.authService = authService;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        String bearerToken = httpServletRequest.getHeader(AUTHORIZATION_HEADER_NAME);
        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            SecurityContextHolder.getContext().setAuthentication(authService.resolveToken(bearerToken.substring(7)));
        }
        chain.doFilter(request, response);
    }
}
