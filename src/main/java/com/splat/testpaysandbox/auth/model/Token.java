package com.splat.testpaysandbox.auth.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Token {
    @JsonProperty("Access-Token")
    private final String accessToken;
    @JsonProperty("expires_in")
    private final long expiresIn;
    private final String scope;

    public Token(String accessToken, long expiresIn, String scope) {
        this.accessToken = accessToken;
        this.expiresIn = expiresIn;
        this.scope = scope;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public long getExpiresIn() {
        return expiresIn;
    }

    public String getScope() {
        return scope;
    }

    @SuppressWarnings("SameReturnValue")
    @JsonProperty("token_type")
    public String getTokenType() {
        return "Bearer";
    }
}
