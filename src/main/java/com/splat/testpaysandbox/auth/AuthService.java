package com.splat.testpaysandbox.auth;

import com.splat.testpaysandbox.auth.model.Token;
import com.splat.testpaysandbox.errors.Errors;
import com.splat.testpaysandbox.errors.TPSException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Service
public class AuthService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthService.class);
    private final AuthProps props;
    private final String scope;

    private final ConcurrentMap<String, TokenMeta> tokens = new ConcurrentHashMap<>();

    @PostConstruct
    public void init() {
        Thread t = new Thread(() -> {
            while (true) {
                tokens.entrySet().removeIf(e -> ZonedDateTime.now().isAfter(e.getValue().expiredAt));
                try {
                    Thread.sleep(5_000);
                } catch (InterruptedException e) {
                    LOGGER.error(e.getMessage(), e);
                }
            }
        }, "tokenCleaner");
        t.setDaemon(true);
        t.start();
    }

    public AuthService(AuthProps props, @Value("${server.url}") String url) {
        this.props = props;
        this.scope = (url.endsWith("/") ? url : url + "/") + "payments/.*";
    }

    public Token getToken(String clientId, String secret) throws TPSException {
        Optional<AuthProps.UserConfiguration> userConfiguration = props.getUsers().stream()
                .filter(uc -> uc.getClientId().equals(clientId) && uc.getSecret().equals(secret))
                .findFirst();
        if (!userConfiguration.isPresent()) {
            throw new TPSException(Errors.AUTHENTIFICATION_FAILURE);
        }

        Token t = userConfiguration.map(uc -> new Token(
                UUID.randomUUID().toString(),
                Optional.ofNullable(uc.getTokenTtl()).orElse(props.getDefaultTokenTtl()).getSeconds(),
                scope
        )).get();

        tokens.put(t.getAccessToken(), new TokenMeta(ZonedDateTime.now().plusSeconds(t.getExpiresIn()), clientId));

        return t;
    }

    public Authentication resolveToken(String token) {
        TokenMeta meta = tokens.get(token);
        if (meta != null && meta.expiredAt.isAfter(ZonedDateTime.now())) {
            return new AnonymousAuthenticationToken(meta.clientId, meta.clientId, Collections.singletonList(new SimpleGrantedAuthority("ROLE_CUSTOMER")));
        }
        return null;
    }

    private static class TokenMeta {
        private final ZonedDateTime expiredAt;
        private final String clientId;

        public TokenMeta(ZonedDateTime expiredAt, String clientId) {
            this.expiredAt = expiredAt;
            this.clientId = clientId;
        }
    }
}
