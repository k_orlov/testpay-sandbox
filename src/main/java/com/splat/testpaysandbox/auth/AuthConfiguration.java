package com.splat.testpaysandbox.auth;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true)
public class AuthConfiguration {

    @Bean
    @ConditionalOnBean(AuthService.class)
    public WebSecurityConfigurerAdapter webSecurityConfigurerAdapter(AuthService authService) {
        return new WebSecurityConfigurer(authService);
    }

    public static class WebSecurityConfigurer extends WebSecurityConfigurerAdapter {
        private final AuthService authService;

        public WebSecurityConfigurer(AuthService authService) {
            this.authService = authService;
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http
                    .httpBasic().disable()
                    .csrf().disable()
                    .anonymous().disable()
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and()
                    .addFilterBefore(new AuthFilter(authService), BasicAuthenticationFilter.class);
        }
    }
}
