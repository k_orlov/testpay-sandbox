package com.splat.testpaysandbox.auth.api;

import com.splat.testpaysandbox.auth.AuthService;
import com.splat.testpaysandbox.auth.model.Token;
import com.splat.testpaysandbox.errors.Errors;
import com.splat.testpaysandbox.errors.TPSException;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Base64;

@RestController
@RequestMapping(
        value = "/oauth2",
        produces = MediaType.APPLICATION_JSON_VALUE
)
public class AuthorizationController {

    private final AuthService authService;

    public AuthorizationController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("/token")
    public Token getToken(
            @RequestParam("grant_type") String grantType,
            @RequestHeader(value = "Authorization", required = false) String base64creds
    ) throws TPSException {
        if (!grantType.equals("client_credentials")
                || base64creds == null || !base64creds.trim().startsWith("Basic ")) {

            throw new TPSException(Errors.INVALID_REQUEST);
        }

        String[] idWithSecret = new String(Base64.getDecoder().decode(base64creds.substring(6))).split(":", 2);
        if (idWithSecret.length != 2) {
            throw new TPSException(Errors.INVALID_REQUEST);
        }
        String clientId = idWithSecret[0];
        String secret = idWithSecret[1];

        return authService.getToken(clientId, secret);
    }
}
