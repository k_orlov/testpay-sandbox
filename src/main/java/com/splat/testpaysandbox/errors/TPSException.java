package com.splat.testpaysandbox.errors;

public class TPSException extends Exception {
    private final Errors error;

    public TPSException(Errors error) {
        this.error = error;
    }

    public Errors getError() {
        return error;
    }
}
