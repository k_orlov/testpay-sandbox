package com.splat.testpaysandbox.errors;

import com.splat.testpaysandbox.errors.api.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> commonExceptionHandler(Exception ex) {
        LOGGER.error(ex.getMessage(), ex);
        return errorToResponse(Errors.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({
            HttpMessageConversionException.class,
            MethodArgumentNotValidException.class,
            MissingServletRequestParameterException.class
    })
    public ResponseEntity<ErrorResponse> badRequestExceptionHandler(Exception ex) {
        return errorToResponse(Errors.INVALID_REQUEST);
    }

    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public ResponseEntity<ErrorResponse> mediaTypeNotSupportedExceptionHandler(Exception ex) {
        return errorToResponse(Errors.UNSUPPORTED_MEDIA_TYPE);
    }

    @ExceptionHandler(AuthenticationCredentialsNotFoundException.class)
    public ResponseEntity<ErrorResponse> authFailureExceptionHandler(Exception ex) {
        return errorToResponse(Errors.AUTHENTIFICATION_FAILURE);
    }

    @ExceptionHandler(TPSException.class)
    public ResponseEntity<ErrorResponse> tpsExceptionHandler(TPSException ex) {
        return errorToResponse(ex.getError());
    }

    private ResponseEntity<ErrorResponse> errorToResponse(Errors error) {
        return new ResponseEntity<>(new ErrorResponse(error), error.getStatus());
    }
}
