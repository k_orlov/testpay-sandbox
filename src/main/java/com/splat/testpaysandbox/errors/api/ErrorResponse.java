package com.splat.testpaysandbox.errors.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.splat.testpaysandbox.errors.Errors;

/**
 * Common response in case of errors
 */
public class ErrorResponse {
    private final Errors error;

    public ErrorResponse(Errors error) {
        this.error = error;
    }

    public Errors getError() {
        return error;
    }

    @JsonProperty("error_description")
    public String getDescription() {
        return error.getDescription();
    }
}
