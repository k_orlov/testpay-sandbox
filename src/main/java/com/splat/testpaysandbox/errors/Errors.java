package com.splat.testpaysandbox.errors;

import org.springframework.http.HttpStatus;

/**
 * Enumeration of possible errors that can be returned by TestPay
 */
public enum Errors {
    INVALID_REQUEST(HttpStatus.BAD_REQUEST, "Request is not well-formatted, syntactically incorrect or violates schema"),
    AUTHENTIFICATION_FAILURE(HttpStatus.UNAUTHORIZED, "Authentication failed due to invalid authentication credentials"),
    UNSUPPORTED_MEDIA_TYPE(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "The server does not support the request payload media type"),
    INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "An internal server errors has occurred");

    private final HttpStatus status;
    private final String description;

    Errors(HttpStatus status, String description) {
        this.status = status;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public HttpStatus getStatus() {
        return status;
    }
}
